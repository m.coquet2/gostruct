/*
	TODO: linkedlist header
*/
package linkedlist

const InitialCapacity int = 64 // the initial node capacity of the structure
const ExpandBy int = 32        // expand the array by this much if it overflows

type LinkedList struct {
	nodeArray []Node // Memory block for nodes
	capacity  int    // the current capacity of the array
	length    int    // the number of items in the array
	current   *Node  // the cursor position within the list
	head      *Node  // the first item in the list
	tail      *Node  // the last item in the list
}

type Node struct {
	prev *Node       // previous node, allow double
	data interface{} // the data of any arbitrary type being stored
	next *Node       // next node, allow double
}

// NewEmpty function will create a new, empty, linked list
//   return:      LinkedList -- return the new empty linked list
func NewEmpty() LinkedList {
	var ll LinkedList
	ll.nodeArray = make([]Node, InitialCapacity)
	return ll
}

// New function initializes a new linked list with the given data in the arguments
//      arg: initList []Node -- initialize a new linked list with the given data
//   return:      LinkedList -- return the new linked list
func New(initList []Node) LinkedList {
	var ll LinkedList
	ll = NewEmpty()
	// set length and capacity variables based on initial list
	ll.length = len(initList)
	if ll.length <= InitialCapacity {
		ll.capacity = InitialCapacity
	} else {
		ll.capacity = len(initList) + ExpandBy
		ll.nodeArray = make([]Node, ll.capacity)
	}

	// put the nodes in the list
	InsertAt(ll, initList, 0)

	return ll
}

// Search cycles the linked list for the given item(s)
//      arg: item interface{} -- the data the user wishes to search for
//   return:           []Node -- if found return here, allow duplicates
func Search(item interface{}) []Node {
	return nil
}

// InsertAt function adds the given item(s) to the list in order, at the given position
//      arg:       ll LinkedList -- linked list to insert into
//             items interface{} -- the item or items to be added to the list in the given order
//                       pos int -- the position to place the new item(s), traverse this number of items and
//                                  place after, allow circular traversal
func InsertAt(ll LinkedList, items interface{}, pos int) {

}

// Insert function adds the given item(s) to the list in order, at the current cursor position
//      arg:       ll LinkedList -- linked list to insert into
//             items interface{} -- the item or items to be added to the list in the given order
func Insert(ll LinkedList, items interface{}) {

}

// Remove function removes the given item(s) from the list if they exist, allow duplicates
//      arg: items []Node -- the item or items to be added to the list in the given order
func Remove(items []Node) {

}

// Print the linked list from the given low index to the high index
//      arg:  low int -- the inclusive range to be printed
//           high int -- the inclusive range to be printed
func Print(low int, high int) {

}

// GetNode function cycles through nodes one at a time until it gets to the index then returns that Node
//      arg:  ll Linkedlist -- the list to operate on
//                index int -- the index of the requested Node
//   return:           Node -- the requested Node
func GetNode(ll LinkedList, index int) Node {
	// avoid overflow
	if index > ll.length {
		index = ll.length
	}

	ll.current = ll.head // go to the start of the list

	for i := 0; i < index; i++ {
		Next(ll)
	}

	return *ll.current
}

// Next function advances the lists cursor to the next node, if it exists
//      arg:  ll Linkedlist -- the list to operate on
func Next(ll LinkedList) {
	if ll.current.next != nil {
		ll.current = ll.current.next
	}
}

// Prev function moves the cursor to the previous node, if it exists
//      arg:  ll Linkedlist -- the list to operate on
func Prev(ll LinkedList) {
	if ll.current.prev != nil {
		ll.current = ll.current.prev
	}
}

// Head function moves the cursor to the head of the list
//      arg:  ll Linkedlist -- the list to operate on
func Head(ll LinkedList) {
	if ll.head != nil {
		ll.current = ll.head
	}
}

// Tail function moves the cursor to the tail of the list
//      arg:  ll Linkedlist -- the list to operate on
func Tail(ll LinkedList) {
	if ll.tail != nil {
		ll.current = ll.tail
	}
}

// Search cycles the linked list for the given item(s)
//      arg: data interface{} -- data for the node
//   return:             Node -- New node
func NewNode(data interface{}) Node {
	var n Node
	n.data = data
	return n
}
