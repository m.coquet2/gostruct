/*
	TODO: linkedlist header
*/

package linkedlist

import (
	"math/rand"
	"reflect"
	"testing"
)

func makeIntList(n int) []Node {
	var d []Node
	d = make([]Node, n)
	d[0] = NewNode(int16(rand.Intn(300)))
	d[0].prev = nil
	for i := 1; i < n-1; i++ {
		d[i] = NewNode(int16(rand.Intn(300)))
	}
	d[0].next = &d[1]
	d[n-1] = NewNode(int16(rand.Intn(300)))
	d[n-1].prev = &d[n-2]
	d[n-1].next = nil
	return d
}

func checkIntList(ll LinkedList, want []Node, t *testing.T) {
	Head(ll)
	// check nodes
	var i int = 0
	for {
		if ll.current.data != want[i].data {
			t.Errorf("checkIntList ll.current.data should contain %v, has %v", want[i].data, ll.current.data)
		}

		// advance the list if possible
		if ll.current.next != nil {
			i++
			Next(ll)
		} else {
			// check if this is actually the tail, if not something is broke
			if ll.current != ll.tail {
				t.Errorf("checkIntList ll.current should be %v, has %v", ll.tail, ll.current)
			}
			break
		}
	}

}

func TestInsertAt(t *testing.T) {
	var testLL = NewEmpty()
	var newNodes []Node
	newNodes = make([]Node, 5)
	for i := 0; i < 5; i++ {
		newNodes[i] = NewNode(i)
		InsertAt(testLL, newNodes[i], i)
	}

	//  check head updated
	if testLL.head != &newNodes[0] {
		t.Errorf("testLL.head should contain %v, has %v", &newNodes[0], testLL.head)
	}

	// check tail updated
	if testLL.tail != &newNodes[4] {
		t.Errorf("testLL.head should contain %v, has %v", &newNodes[4], testLL.tail)
	}

	// check capacity
	if testLL.capacity != InitialCapacity {
		t.Errorf("testLL.capacity should contain %v, has %v", InitialCapacity, testLL.capacity)
	}

	// check length updated
	if testLL.length != 5 {
		t.Errorf("testLL.length should contain 5, has %v", testLL.length)
	}

	// check nodes and connections
	checkIntList(testLL, newNodes, t)

	// Test inserting an item at the head
	InsertAt(testLL, NewNode(32), 0)

	// check first node
	var tNode Node = GetNode(testLL, 0)
	var tNodeNext Node = GetNode(testLL, 1)
	if tNode.data != 32 {
		t.Errorf("node[%v].data should contain %v, has %v", 0, 32, tNode.data)
	}
	if tNode.prev != nil {
		t.Errorf("node[%v].prev should contain nil, has %v", 0, tNode.prev)
	}
	if tNode.next != &tNodeNext {
		t.Errorf("node[%v].next should contain %v, has %v", 0, &tNodeNext, tNode.next)
	}

	// check length updated
	if testLL.length != 6 {
		t.Errorf("testLL.length should contain 6, has %v", testLL.length)
	}

	var tSlice []Node
	copy(tSlice, newNodes)
	newNodes = nil
	newNodes = append(newNodes, tNode)
	newNodes = append(newNodes, tSlice...)
	// check nodes and connections
	checkIntList(testLL, newNodes, t)

	// Test inserting an item at the tail
	var tNode0 Node = NewNode(25)
	InsertAt(testLL, tNode0, testLL.length)

	// check tail
	Tail(testLL)

	if testLL.tail != &tNode0 {
		t.Errorf("testLL.tail should be %v, has %v", &tNode0, testLL.tail)
	}

	// check length updated
	if testLL.length != 7 {
		t.Errorf("testLL.length should contain 7, has %v", testLL.length)
	}

	newNodes = append(newNodes, tNode0)

	// check nodes and connections
	checkIntList(testLL, newNodes, t)

	// Test inserting an item someplace in between
	var tNode1 Node = NewNode(43)
	InsertAt(testLL, tNode0, 4)

	tSlice = nil
	tSlice = append(tSlice, newNodes[0:3]...)
	tSlice = append(tSlice, tNode1)
	tSlice = append(tSlice, newNodes[4:]...)
	newNodes = nil
	copy(newNodes, tSlice)

	var gotNode Node = GetNode(testLL, 4)

	// check node at index 4 should be the new node
	if gotNode.data != tNode1.data {
		t.Errorf("gotNode.data should contain %v, has %v", tNode1.data, gotNode.data)
	}

	// check length updated
	if testLL.length != 8 {
		t.Errorf("testLL.length should contain 8, has %v", testLL.length)
	}

	// Test clearing the list and inserting again on an empty list with an overflowed index
	testLL = NewEmpty()
	// check length updated
	if testLL.length != 0 {
		t.Errorf("testLL.length should contain 0, has %v", testLL.length)
	}

	var tNode2 Node = NewNode(14)
	InsertAt(testLL, tNode2, 3) // should just put it at the head since there is no 3

	// check length updated
	if testLL.length != 1 {
		t.Errorf("testLL.length should contain 1, has %v", testLL.length)
	}

	//  check head updated
	if testLL.head != &tNode2 {
		t.Errorf("testLL.head should contain %v, has %v", &tNode2, testLL.head)
	}

	newNodes = nil
	newNodes = append(newNodes, tNode2)

	// check nodes and connections
	checkIntList(testLL, newNodes, t)

	// Test inserting until the capicity is reached to see if it expands the list storage
	testLL = NewEmpty()
	newNodes = nil
	for i := 0; i < InitialCapacity; i++ {
		newNodes = append(newNodes, NewNode(rand.Intn(100)))
	}
	InsertAt(testLL, newNodes, 0)
	var tNode3 Node = NewNode(101)
	newNodes = append(newNodes, tNode3)
	// insert 1 more
	InsertAt(testLL, tNode3, testLL.length)

	// check length updated
	if testLL.length != InitialCapacity+1 {
		t.Errorf("testLL.length should contain %v, has %v", InitialCapacity+1, testLL.length)
	}

	// check nodes and connections
	checkIntList(testLL, newNodes, t)
}

func TestRemove(t *testing.T) {
	type args struct {
		items []Node
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Remove(tt.args.items)
		})
	}
}

func TestPrint(t *testing.T) {
	type args struct {
		low  int
		high int
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			Print(tt.args.low, tt.args.high)
		})
	}
}

func TestSearch(t *testing.T) {
	type args struct {
		item interface{}
	}
	tests := []struct {
		name string
		args args
		want []interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Search(tt.args.item); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewEmpty(t *testing.T) {
	var ll LinkedList
	ll = NewEmpty()

	if ll.nodeArray == nil {
		t.Errorf("TestEmpty(): LinkedList is nil")
	}
}

func TestNew(t *testing.T) {
	var ll LinkedList
	var testdata []Node

	type args struct {
		initList []Node
	}

	testdata = makeIntList(5)
	ll = New(testdata)

	tests := []struct {
		name string
		got  Node
		want Node
	}{
		{"TestNew: 0", ll.nodeArray[0], testdata[0]},
		{"TestNew: 1", ll.nodeArray[1], testdata[1]},
		{"TestNew: 2", ll.nodeArray[2], testdata[2]},
		{"TestNew: 3", ll.nodeArray[3], testdata[3]},
		{"TestNew: 4", ll.nodeArray[4], testdata[4]},
	}

	for i, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if tt.got.data != tt.want.data {
				t.Errorf("nodeArray[%v].data should contain %v, has %v", i, tt.want.data, tt.got.data)
			}
			if tt.got.prev != tt.want.prev {
				t.Errorf("nodeArray[%v].prev should contain %v, has %v", i, tt.want.prev, tt.got.prev)
			}
			if tt.got.next != tt.want.next {
				t.Errorf("nodeArray[%v].next should contain %v, has %v", i, tt.want.next, tt.got.next)
			}
		})
	}
}

func Test_Expanding(t *testing.T) {
	var ll LinkedList

	const setcapacity int = 64
	ll.capacity = setcapacity

	var testdata []int16
	var testnodes []Node
	testdata = make([]int16, setcapacity+1)
	testnodes = make([]Node, setcapacity+1)
	for i := 0; i < setcapacity; i++ {
		testdata[i] = int16(rand.Intn(32767))
		testnodes[i] = NewNode(testdata[i])
	}

	testdata[setcapacity] = int16(rand.Intn(32767))
	testnodes[setcapacity] = NewNode(testdata[setcapacity])
	ll = New(testnodes) // shouldnt panic or get any errors

	// check capacity
	if ll.capacity != setcapacity+ExpandBy+1 {
		t.Errorf("capacity variable should contain %v, has %v", setcapacity+ExpandBy+1, ll.capacity)
	}

	// check length
	if ll.length != setcapacity+1 {
		t.Errorf("length variable should contain %v, has %v", setcapacity+1, ll.length)
	}

	// check node storage
	for i := 0; i < setcapacity; i++ {
		// check data
		if ll.nodeArray[i].data != testdata[i] {
			t.Errorf("nodeArray[%v].data should contain %v, has %v", i, testdata[i], ll.nodeArray[i].data.(int16))
		}
		// check prev
		if ll.nodeArray[i].prev != nil {
			t.Errorf("nodeArray[%v].data should be nil, has %v", i, ll.nodeArray[i].prev)
		}
		// check next
		if ll.nodeArray[i].next != nil {
			t.Errorf("nodeArray[%v].data should be nil, has %v", i, ll.nodeArray[i].next)
		}
	}
}

func Test_newNode(t *testing.T) {
	var testData int16 = 12
	var testNode Node = NewNode(testData)
	var gotData int16 = testNode.data.(int16)
	tests := []struct {
		name string
		data interface{}
		want interface{}
	}{
		{"new", gotData, testData},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewNode(tt.data); !reflect.DeepEqual(got.data, tt.want) {
				t.Errorf("newNode() = %v, want %v", got.data, tt.want)
			}
		})
	}
}
